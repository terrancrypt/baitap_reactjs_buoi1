import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className='bg-dark py-5'>
        <span className='text-white'>Copyright © Your Website 2023</span>
      </div>
    )
  }
}
