import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";

export default class Body extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="py-5">
            <Banner />
          </div>
          <div className="row pb-5">
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
          </div>
          <div className="row pb-5">
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
            <div className="col-3"><Item/></div>
          </div>
        </div>
      </div>
    );
  }
}
