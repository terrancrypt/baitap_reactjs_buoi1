import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div
        className="row flex-lg-row-reverse align-items-center py-5 bg-light"
        bis_skin_checked={1}
      >
        <div className="col-10 col-sm-8 col-lg-6" bis_skin_checked={1}>
          <img
            src="https://getbootstrap.com/docs/5.3/examples/heroes/bootstrap-themes.png"
            className="d-block mx-lg-auto img-fluid"
            alt="Bootstrap Themes"
            width={700}
            height={500}
            loading="lazy"
          />
        </div>
        <div className="col-lg-6" bis_skin_checked={1}>
          <h1 className="display-5 fw-bold lh-1 mb-3">
            Responsive left-aligned hero with image
          </h1>
          <p className="lead">
            Quickly design and customize responsive mobile-first sites with
            Bootstrap, the world’s most popular front-end open source toolkit,
            featuring Sass variables and mixins, responsive grid system,
            extensive prebuilt components, and powerful JavaScript plugins.
          </p>
          <div
            className="d-grid gap-2 d-md-flex justify-content-md-start"
            bis_skin_checked={1}
          >
            <button
              type="button"
              className="btn btn-primary btn-lg px-4 me-md-2"
            >
              Primary
            </button>
            <button
              type="button"
              className="btn btn-outline-secondary btn-lg px-4"
            >
              Default
            </button>
          </div>
        </div>
      </div>
    );
  }
}
