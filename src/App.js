import './App.css';
import BaiTapComponents from './components/BaiTapLayoutComponent/BaiTapComponents';

function App() {
  return (
    <div className="App">
      <BaiTapComponents/>
    </div>
  );
}

export default App;
